let objectProject = {
  index: {
    entry: "src/main/index.js",
    template: "public/index.html",
    filename: "index.html",
    title: "臺中市政府智慧防汛",
    chunks: ["chunk-vendors", "chunk-common", "index"]
  }
}

let page = {}
let projectname = process.argv[3]
if (process.env.NODE_ENV == "development") {
  page = objectProject
} else {
  page[projectname] = objectProject[projectname]
}

module.exports = {
  assetsDir: "./static",
  publicPath: "",
  //outputDir: "dist" + projectname,
  pages: objectProject,
  filenameHashing: true,
  productionSourceMap: false,
  transpileDependencies: [
    "vuetify"
  ],
  devServer:{
    proxy: {
      "/api/javaapi/": {
        target: "http://127.0.0.1:8111/javaapi/",
        ws: false,
        changeOrigin: true,
        pathRewrite: {
          "^/api/javaapi/": ""
        }
      },
      "/api/8210/": {
        target: "http://127.0.0.1:8111/8210/",
        ws: false,
        changeOrigin: true,
        pathRewrite: {
          "^/api/8210/": ""
        }
      },
      "/api/8899/": {
        target: "http://127.0.0.1:8111/8899/",
        ws: false,
        changeOrigin: true,
        pathRewrite: {
          "^/api/8899/": ""
        }
      }
    }
  }
}