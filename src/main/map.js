import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import Vuex from 'vuex';
import vuetify from '@/plugins/vuetify';

import router from '@/router/index.js'

import index from '@/layouts/index.vue'

Vue.config.productionTip = false

Vue.use(VueAxios, axios)
Vue.use(Vuex)
new Vue({
  vuetify,
  router,
  render: h => h(index)
}).$mount('#app')
