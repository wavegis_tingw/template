import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import Vuex from 'vuex';
import vuetify from '@/plugins/vuetify';

import VueLayers from 'vuelayers'

import 'vuelayers/lib/style.css'

import Echarts from 'echarts'

import router from '@/router/index.js'

import index from '@/layouts/index.vue'



Vue.config.productionTip = false

Vue.use(VueAxios, axios)
Vue.use(Vuex)
Vue.use(VueLayers, {
  dataProjection: 'EPSG:4326',
})
Vue.use(Echarts)
new Vue({
  vuetify,
  router,
  render: h => h(index)
}).$mount('#app')
