import Vue from 'vue'
import Router from 'vue-router'

const files = require.context('@/layouts/work',true,/\.vue$/)

const modules = [];

modules.push({
  path: '/',
  name: 'home',
  redirect: 'index',
})

files.keys().forEach(key => {
 // console.log(key.split('.')[1])
  const layoutPath = key.split('.')[1]
  const layoutName = key.split('/').pop().replace(/\.\w+$/, '')
  const layoutModule = files(key)
  if(layoutName == 'IndexCard'){
    modules.push({
      path: '/index',
      name: 'index',
      component: layoutModule.default || layoutModule
    })
  }else{
    modules.push({
      path: layoutPath,
      name: layoutName,
      component: layoutModule.default || layoutModule
    })
  }

})

Vue.use(Router)

export default new Router({
  mode: 'history',
  //mode: 'hash',
  base: '/taichung_mobile',
  routes: modules,
})
